import React from 'react';
import SearchBox from './SearchBox';
import ReposTable from './ReposTable';

class ReposList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            columnDefs: []
        };
    }

    updateData = ({data}) => {
        if (data) {
            const {name,company,email,followers,updated_at,avatar_url} = data;
            const filteredData = {name,company,email,followers,updated_at,avatar_url};

            let rows = Object.values(filteredData);
            let columnDefs = Object.keys(filteredData);
            this.setState({rows,columnDefs});
        }

    };

    render() {
        const {rows,columnDefs} = this.state;
        return (
            <div className="repos_list_wrapper">
                <SearchBox data={this.updateData}/>
                {rows.length > 0 ? <ReposTable rows={rows} columnDefs={columnDefs}/> : null}
            </div>
        )
    }
}


export default ReposList;