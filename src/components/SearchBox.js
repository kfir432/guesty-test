import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import api from '../api/api';

class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text:''
        };
    }

    submitForm = async(e) => {
        e.preventDefault();
        const {text} = this.state;
        let userData = await api.get(`/users/${text}`);
        this.props.data(userData);
    };
    handleChange =  (e) => {
        let text = e.target.value;
        this.setState({text});
    };

    render() {
        const {text} = this.state;
        return (
            <form onSubmit={this.submitForm}>
                <div className="form_wrapper">
                    <div>
                        <TextField
                            id="standard-full-width"
                            label="Enter github user"
                            style={{margin: 8}}
                            placeholder="Placeholder"
                            fullWidth
                            margin="normal"
                            value={text}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div>
                        <Button variant="contained" type="submit" color="primary">
                            Search
                        </Button>
                    </div>
                </div>
            </form>
        )
    }
}

export default SearchBox;