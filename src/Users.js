import React from 'react';
import api from "./api/api";

class Users extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            repos:[]
        }
    }
    fetchRepos = async() =>{
        const userId = this.props.match.params.id;
        let userRepos = await api.get(`/users/${userId}/repos`);
        console.log(userRepos);
    }
    componentWillMount() {
        this.fetchRepos();
    }

    render(){
        return(
            <div>user</div>
        )
    }
}

export default Users;