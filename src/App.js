import React from 'react';
import './style.scss';
import ReposList from './components/ReposList';

function App() {
    return (
        <div className="main_wrapper">
            <div>
                <ReposList/>
            </div>
        </div>
    );
}

export default App;
